
# Fazztrack 1


## Setup Localhost
Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/ebyantoo/express-fazztrack-1.git
```
```bash
npm install
```

To start the express server, run the following

```bash
node index.js
```

## Setup Docker
You can also run this app as a Docker container:

Step 1: Clone the repo

```bash
  git clone https://gitlab.com/ebyantoo/express-fazztrack-1.git
  cd express-fazztrack-1
```

Step 2: Create Dockerfile

```bash

FROM node:16-alpine

WORKDIR /app

COPY index.js index.js
COPY package.json package.json

RUN npm install
 
CMD [ "node", "./index.js"]

```

Step 3: Build the Docker image

```bash
docker build -t fazztrack-node-1:latest .
```

Step 4: Run the Docker container locally:

```bash
docker run -p 3000:3000 -d fazztrack-node-1:latest
```

## Setup Kubernetes Deployment
Kubernetes, also known as K8s, is an open source system for managing containerized applications across multiple hosts. It provides basic mechanisms for deployment, maintenance, and scaling of applications.

Step 1 : Defining Container POD With Deployment

```bash

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nodejs-fazztrack
  namespace: default
  labels:
    app: nodejs-fazztrack
spec:
  selector:
    matchLabels:
      app: nodejs-fazztrack
  replicas: 2
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: nodejs-fazztrack
    spec:
      # initContainers:
      # Init containers are exactly like regular containers, except:
      # - Init containers always run to completion.
      # - Each init container must complete successfully before the next one starts.
      containers:
        - name: nodejs-fazztrack
          image: ebyantoo/nodejs-fazztrack:latest
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
            limits:
              cpu: 100m
              memory: 100Mi
          envFrom:
            - configMapRef:
                name: config-nodejs
          ports:
            - containerPort: 3000
              protocol: TCP
```

Step 2 : Read Environtment From ConfigMap

```bash

kind: ConfigMap
apiVersion: v1
metadata:
  name: config-nodejs
  namespace: default
data:
  APPLICATION_NAME: PROGRAM FAZZTRACK 2
  ENVIRONTMENT: DEVELOPMENT

```

Step 2 : Jalankan Perintah Kubectl

```bash

kubectl apply -f deployment.yaml
kubectl apply -f configmap.yaml

```